var kue = require('kue');
var jobs = kue.createQueue();

function queueOrder(order) {
	jobs.create('order', order).save();
}

exports.handleMessage = function (message) {
	message = message.trim();
	var fragments = message.split(" ");
	var command = fragments[0];
	if (command == "order") {
		var order = {
			name : fragments[1],
			symbol : fragments[2],
			original : fragments[3],
			remaining : fragments[4],
			buyorsell : fragments[5],
			type : fragments[6],
			price : fragments[7],
			time : fragments[8]
		};
		queueOrder(order);
	}
	console.log(order);
}

jobs.process('order', function(job, done){

});
