var kue = require('kue');
var jobs = kue.createQueue();
var stdin = process.openStdin();
var net = require('net');
var async = require('async');
var postal  = require('postal');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/GD');
var playerInformation = require('./models/playerInformation');
var connectOrder = require('./models/connectOrder');
var kldk = require('./models/kldk');
var kltt = require('./models/kltt');
var Game = require('./models/Game');
var db_Player = require('./models/db_Player');
var db_Group = require('./models/db_Group');

stdin.on('data', function(message) {
  handleMessage(message.toString());
});

// var channel = postal.channel();
var status = ["timeSleep", "timeATO", "timeKLTT", "timeATC"];
var next = {
  number: 0,
  status: "timeSleep"
};
var start = false;
var userInformation = {
  _id: 0,
  group_id: 0,
  invite_id:0
};

(var subcription = postal.subscribe({
  channel : "addNew",
  topic   : "kltt",
  callback : function(data, e){
    
  }
})();

function handleMessage(message) {
  message = message.trim();
  var fragments = message.split(" ");
  var database = fragments[0];
  var command = fragments[1];
  var gameInformation = {
    gameCode: 0,
    phienT: 0
  };

  if (start) {
    if (database == "order") {

      var order = {
        name: fragments[2],
        symbol: fragments[3],
        original: fragments[4],
        remaining: fragments[5],
        buyorsell: (fragments[6] === 'true'),
        type: fragments[7],
        price: fragments[8],
        time: new Date(),
        phienT: gameInformation.phienT,
        gameCode: gameInformation.gameCode
      };

      if (command == "order" && next.status == "kltt") {
        queueOrder(order);
      } else if (command == "order" && (next.status == "ATO" || next.status == "ATC")) {
        queueAddOrder(order);
      } else if (command == "order" && next.status == "sleep") {
        console.log("ngu ngu");
      } else if (command == "next") {
        next.number = (next.number + 1) % 4;
        next.status = status[(next.number + 1) % 4];
        console.log("Start " + next.status + " time");
        if (next.status == "sleep" || next.status == "kltt") {
          getStock(function(err, data) {
            for (var i = 0; i < data.length; i++) {
              queueKhopLDK(data[i]);
            }
          });
        }
      } else if (command == "createGroup") {
        console.log("");
      } else {
        console.log("ngu vua thoi");
      }
    } else if (database == "player") {
      var player = {
        userName: fragments[2],
        password: fragments[3],
        balance: 0,
        group: null,
        inviteList: [],
        requestList: [],
        gameCode: gameInformation.gameCode
      };

      if (command == "addSingle") {
        player.balance = 50000000;
        queueAddPlayer(player);
      } else if (command == "addSuper") {
        player.balance = 200000000;
        queueAddPlayer(player);
      } else if (command == "listPlayer") {
        db_Player.listPlayer(function(err, data) {
          console.log(data);
        });
      } else if (command == "use") {
        db_Player.listOne(player, function(err, pl) {
          userInformation._id = pl[0]._id;
          console.log(pl);
        });
      } else if(command == "invite"){
        db_Player.listOne(player, function(err, pl) {
          userInformation.invite_id = pl[0]._id;
          console.log(pl);
        });
      }else{
        console.log("Tam ngu si");
      }
    } else if (database == "group") {
      var group = {
        groupName: fragments[2],
        captain: userInformation._id,
        groupMember: [userInformation._id],
        inviteList: [],
        requestList: []
      }

      if (command == "listGroup") {
        db_Group.listGroup(function(err, gr) {
          console.log(gr);
        });
      } else if (command == "createGroup") {
        db_Group.createGroup(group, function(err, gr) {
          console.log("group id: " + gr._id);
          console.log("group name: " + gr.groupName);
        });
      } else if (command == "addRequestGroup") {
        db_Group.addRequestGroup(group, userInformation, function(err, gr) {
          console.log(gr);
        });
      } else if (command == "removeRequestGroup") {
        db_Group.removeRequestGroup(group, userInformation, function(err, gr) {
          console.log(gr);
        });
      } else if (command == "use") {
        db_Group.listOne(userInformation, function(err, pl) {
          userInformation.group_id = pl[0]._id;
          console.log(pl);
        });
      } else if (command == "addInvite") {
        db_Group.addInviteGroup(group, userInformation, function(err, gr){
          console.log(gr);
        });
      } else if(command == "removeInvite"){
        db_Group.removeInviteGroup(group, userInformation, function(err, gr){
          console.log(gr);
        })
      }else{
        console.log("no group");
      }
    } else {
      console.log("alo alo");
    }
  }

  if (database == "start" && !start) {
    start = true;
    Game.find({}).exec(function(err, data) {
      next.status = data[0].status;
      for (var i = 0; i < status.length; i++) {
        if (next.status == status[i]) {
          next.number = i;
          break;
        }
      }
      gameInformation.gameCode = data[0].gameCode;
      gameInformation.phienT = data[0].phienT;
      timeOut(data[0], next, status);
    });
  } else if (!start) {
    console.log(start);
    console.log("Must start!");
  }
}


var timeOut = function timeOut(data, next1, status1) {
  setTimeout(function() {
    next1.number = (next1.number + 1) % 4;
    next1.status = status1[next1.number % 4];
    console.log("Start " + next.status + " time");
    timeOut(data, next1, status1);
  }, data[next1.status] * 1000);
}

var queueAddPlayer = function queueAddPlayer(player) {
  jobs.create('addPlayer', player).save();
}

jobs.process("addPlayer", function(job, done) {
  db_Player.addPlayer(job.data, function(err, player) {
    console.log("success");
    console.log("userID: " + player._id)
    done();
  });
});

var queueAddOrder = function queueAddOrder(order) {
  jobs.create('addOrder', order).save();
}

jobs.process("addOrder", function(data, done) {
  connectOrder.addOrder(data.data, function() {
    done();
  });
});

var queueOrder = function queueOrder(order) {
  jobs.create('kltt', order).save();
}

jobs.process("kltt", function(data, done) {
  kltt.findMatchableOrders(data.data, function(err, orders) {
    var dirty = klt.matchOrders(data.data, orders);
    kltt.saveAll(dirty, function() {
      done();
    });
  });
});

/****************************************************************************************************************************/

var queueKhopLDK = function queueKhopLDK(data) {
  jobs.create('kldk', data).save();
}

jobs.process('kldk', function(job, done) {
  kldk.mapReduceFunction(job.data, function(err, data1) {
    kldk.mapReduceATO(job.data, function(err, data2) {
      kldk.augmentOrder(data1, data2, function() {
        var information = kldk.checkKey();
        kldk.minorFocus(job.data, information, function() {
          kldk.majorFocus(job.data, information, function() {
            kltt.saveAll([], function() {
              done();
            });
          });
        });
      });
    });
  });
});