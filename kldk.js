var kue = require('kue');
var jobs = kue.createQueue();
var stdin = process.openStdin();
var net = require('net');
var async = require('async');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/GD');
var Order = require('./models/Order');
var Stock = require('./models/Stock');

stdin.on('data', function(message) {
  handleMessage(message.toString());
});

function handleMessage(message) {
  message = message.trim();
  var fragments = message.split(" ");
  var command = fragments[0];
  var order = {
    name: fragments[1],
    symbol: fragments[2],
    original: fragments[3],
    remaining: fragments[4],
    buyorsell: (fragments[5] === 'true'),
    type: fragments[6],
    price: fragments[7],
    time: new Date()
  };
  if (command == "order") {
    queueAddLDK(order);
  }else if(command == "kldk"){   
    getStock(function(err, data){
      for(var i=0; i < data.length; i++){
        queueKhopLDK(data[i]);
      }
    }); 
  }else{
    console.log("!order");
  }
}

var getStock = function getStock(callback){
  Stock.find().exec(callback);
}

var queueAddLDK = function queueKLDK(order){
  jobs.create('altt', order).save();
}

var queueKhopLDK = function queueKhopLDK(data){
  jobs.create('kldk', data).save();
}

jobs.process("altt", function (job, done) {
  add(job.data, function(){
    done();
  });
});

var add = function add(order, callback) {
  Order.create(order, callback);
}

jobs.process('kldk', function(job, done){
  mapReduceFunction(job.data, function(err, data1){
    mapReduceATO(job.data, function(err, data2){
      augmentOrder(data1, data2 , function(){
        var information = checkKey();
        minorFocus(job.data, information, function(){
          majorFocus(job.data, information, function(){
            saveAll([], function(){
            console.log(dirty);
            done();
            });
          });
        });
      });
    });
  });
});

var mapReduceFunction = function mapReduceFunction(data, callback){
  var  o = {};
  o.map = function(){
    var key = {
      price : this.price,
      buyorsell : this.buyorsell,
      type : this.type
    };
    var value = {remaining : this.remaining};
    emit(key , this.remaining);
  }

  o.query = {
    symbol : data.symbol,
    type : 1
  };

  o.reduce = function(k, v){
    return Array.sum(v);
  }

  Order.mapReduce(o , callback);
}

var mapReduceATO = function mapReduceATO(data,  callback){
  var  o = {};
  o.map = function(){
    var key = {
      price : this.price,
      buyorsell : this.buyorsell
    };
    var value = {remaining : this.remaining};
    emit(key , this.remaining);
  }

  o.query = {
    symbol : data.symbol,
    type : 0
  };

  o.reduce = function(k, v){
    return Array.sum(v);
  }

  Order.mapReduce(o , callback);
}

var buyOrder = [];
var sellOrder = [];
var keyOrder = [];
var dirty = [];

var newArray = function newArray(array, count, data){
  array[count] = {};
  array[count]._id = {};
  array[count].value = data.value;
  array[count]._id.price = data._id.price;
  array[count]._id.buyorsell = data._id.buyorsell;
}

var augmentOrder = function augmentOrder(data, data2, callback){
  var countBuy = -1;
  var countSell = -1;
  buyOrder = [];
  sellOrder = [];
  var buyATO = 0;
  var sellATO = 0;
  for(var i = 0; i < data2.length; i++){
    if(data2[i]._id.buyorsell){
      buyATO = data2[i].value;
    }else{
      sellATO = data2[i].value;
    }
  }

  for(var i = 0; i < data.length; i++){
    if(data[data.length-i-1]._id.buyorsell){
      if(countBuy == -1){
        countBuy ++;
        newArray(buyOrder, countBuy, data[data.length-i-1]);
        buyOrder[countBuy]._id.buyorsell = data[data.length-i-1]._id.buyorsell + buyATO;
      }else{
        countBuy ++;
        newArray(buyOrder, countBuy, data[data.length-i-1]);
        buyOrder[countBuy].value = buyOrder[countBuy-1].value + data[data.length-i-1].value;
      } 
    }else{
      if(i < data.length-1){
        if(data[data.length-i-1]._id.price != data[data.length-i-2]._id.price){
          if(countBuy == -1){

            countBuy ++;
            newArray(buyOrder, countBuy, data[data.length-i-1]);
            buyOrder[countBuy]._id.buyorsell = true;
            buyOrder[countBuy].value = buyATO;
          }else if(countBuy > 0){
            if(data[data.length-i-1]._id.price != buyOrder[countBuy]._id.price){
              countBuy++;
              newArray(buyOrder, countBuy, data[data.length-i-1]);
              buyOrder[countBuy]._id.buyorsell = true;
              buyOrder[countBuy].value = buyOrder[countBuy-1].value;
            }
          }
        }
      }
    }

    if(!data[i]._id.buyorsell){

      if(countSell == -1){
        countSell ++;
        newArray(sellOrder, countSell, data[i]); 
        sellOrder[countSell].value = data[i].value + sellATO;
      }else{
        countSell ++; 
        newArray(sellOrder, countSell, data[i]); 
        sellOrder[countSell].value = sellOrder[countSell-1].value + data[i].value;
      }
    }else{
      if(i < data.length-1){
        if(data[i]._id.price != data[i+1]._id.price){
          if(countSell == -1){
            countSell ++;
            newArray(sellOrder, countSell, data[i]); 
            sellOrder[countSell].value = sellATO;
          }else if(countSell > 0){
            if(data[i]._id.price != sellOrder[countSell]._id.price){
              countSell++;
              newArray(sellOrder, countSell, data[i]); 
              sellOrder[countSell].value = sellOrder[countSell-1].value;
            }
          }
        }
      }
    }
  }

  callback();
}

var checkKey = function checkKey(){
  keyOrder = [];
  var information = {
    max : 0, 
    price : 0, 
    buyorsell: false
  }
  for(var i = 0; i < buyOrder.length; i++){
    if(sellOrder[i].value > buyOrder[buyOrder.length-i-1].value){
      keyOrder[i] = buyOrder[buyOrder.length-i-1];
    }else{
      keyOrder[i] = sellOrder[i];
    }
  }

  for(var i = 0; i < keyOrder.length; i++){
    if(keyOrder[i].value > information.max){
      information.max = keyOrder[i].value;
      information.price = keyOrder[i]._id.price;
      information.buyorsell = keyOrder[i]._id.buyorsell;
    }
  }
  return information;
}

var minorFocus = function minorFocus(data, information ,callback){
  var conditions = {};

  if(information.buyorsell){
    conditions = {
      price : {$gte : information.price},
      orderBy : 1
    }
  }else{
    conditions = {
      price : {$lte : information.price},
      orderBy : -1
    }
  }

  Order.find({
    symbol: data.symbol,
    buyorsell : information.buyorsell,
    remaining: {$gt : 0},
    $or: [
    {price: conditions.price}, 
    {type : 0}
    ]
  }).sort({
    type: 1,
    price: conditions.orderBy,
    time: 1,
    remaining: -1
  }).exec(function(err, data){
    for(var i=0; i < data.length; i++){
      dirty.push(data[i]);
      dirty[i].remaining = 0;
    }
    console.log(information);
    callback();
  });

}

var majorFocus = function majorFocus(data1, information ,callback){
  var conditions = {};
  var copyInformation = information;

  if(information.buyorsell){
    conditions = {
      price : {$lte : information.price},
      orderBy : 1
    }
  }else{
    conditions = {
      price : {$gte : information.price},
      orderBy : -1
    }
  }

  Order.find({
    symbol: data1.symbol,
    buyorsell : !information.buyorsell,
    remaining: {$gt : 0},
    $or: [
    {price: conditions.price}, 
    {type : 0}
    ]
  }).sort({
    type: 1,
    price: conditions.orderBy,
    time: 1,
    remaining: -1
  }).exec(function(err, data){
    for(var i = 0; i < data.length ; i++){
      if(copyInformation.max > 0){
        if(copyInformation.max > data[i].remaining){
          copyInformation.max -= data[i].remaining;
          dirty.push(data[i]);
          dirty[dirty.length -1].remaining = 0;
        }else{
          dirty.push(data[i]);
          dirty[dirty.length -1].remaining -= copyInformation.max;
          copyInformation.max = 0;
        }
      }
    }
    callback();
  });
}

var saveAll = function saveAll(records, callback) {
  async.forEach(records, function (record , cb) {
    console.log(record);
    if (typeof record.save === "function") {
      record.save(cb);
    } else {
      Order(record).save(cb);
    }
  }, callback);
}
