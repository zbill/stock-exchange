var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var groupSchema = new Schema({
  groupName: String,
  captain: String,
  groupMember: [{type: String}],
  inviteList: [{type: String}],
  requestList: [{type: String}]

});

var Group = mongoose.model('group', groupSchema, "group");

module.exports = Group;
