var buyOrder = [];
var sellOrder = [];
var keyOrder = [];
var dirtyKLDK = [];

var mapReduceFunction = function mapReduceFunction(data, callback) {
  var o = {};
  o.map = function() {
    var key = {
      price: this.price,
      buyorsell: this.buyorsell,
      type: this.type
    };
    var value = {
      remaining: this.remaining
    };
    emit(key, this.remaining);
  }

  o.query = {
    symbol: data.symbol,
    type: 1
  };

  o.reduce = function(k, v) {
    return Array.sum(v);
  }

  Order.mapReduce(o, callback);
}

var mapReduceATO = function mapReduceATO(data, callback) {
  var o = {};
  o.map = function() {
    var key = {
      price: this.price,
      buyorsell: this.buyorsell
    };
    var value = {
      remaining: this.remaining
    };
    emit(key, this.remaining);
  }

  o.query = {
    symbol: data.symbol,
    type: 0
  };

  o.reduce = function(k, v) {
    return Array.sum(v);
  }

  Order.mapReduce(o, callback);
}

var newArray = function newArray(array, count, data) {
  array[count] = {};
  array[count]._id = {};
  array[count].value = data.value;
  array[count]._id.price = data._id.price;
  array[count]._id.buyorsell = data._id.buyorsell;
}

var augmentOrder = function augmentOrder(data, data2, callback) {
  var countBuy = -1;
  var countSell = -1;
  buyOrder = [];
  sellOrder = [];
  var buyATO = 0;
  var sellATO = 0;
  for (var i = 0; i < data2.length; i++) {
    if (data2[i]._id.buyorsell) {
      buyATO = data2[i].value;
    } else {
      sellATO = data2[i].value;
    }
  }

  for (var i = 0; i < data.length; i++) {
    if (data[data.length - i - 1]._id.buyorsell) {

      if (countBuy == -1) {
        countBuy++;
        newArray(buyOrder, countBuy, data[data.length - i - 1]);
        buyOrder[countBuy]._id.buyorsell = data[data.length - i - 1]._id.buyorsell + buyATO;
      } else {
        countBuy++;
        newArray(buyOrder, countBuy, data[data.length - i - 1]);
        buyOrder[countBuy].value = buyOrder[countBuy - 1].value + data[data.length - i - 1].value;
      }
    } else {
      if (i < data.length - 1) {
        if (data[data.length - i - 1]._id.price != data[data.length - i - 2]._id.price) {
          if (countBuy == -1) {
            countBuy++;
            newArray(buyOrder, countBuy, data[data.length - i - 1]);
            buyOrder[countBuy]._id.buyorsell = true;
            buyOrder[countBuy].value = buyATO;
          } else if (countBuy > 0) {
            if (data[data.length - i - 1]._id.price != buyOrder[countBuy]._id.price) {
              countBuy++;
              newArray(buyOrder, countBuy, data[data.length - i - 1]);
              buyOrder[countBuy]._id.buyorsell = true;
              buyOrder[countBuy].value = buyOrder[countBuy - 1].value;
            }
          }
        }
      }
    }

    if (!data[i]._id.buyorsell) {

      if (countSell == -1) {
        countSell++;
        newArray(sellOrder, countSell, data[i]);
        sellOrder[countSell].value = data[i].value + sellATO;
      } else {
        countSell++;
        newArray(sellOrder, countSell, data[i]);
        sellOrder[countSell].value = sellOrder[countSell - 1].value + data[i].value;
      }
    } else {
      if (i < data.length - 1) {
        if (data[i]._id.price != data[i + 1]._id.price) {
          if (countSell == -1) {
            countSell++;
            newArray(sellOrder, countSell, data[i]);
            sellOrder[countSell].value = sellATO;
          } else if (countSell > 0) {
            if (data[i]._id.price != sellOrder[countSell]._id.price) {
              countSell++;
              newArray(sellOrder, countSell, data[i]);
              sellOrder[countSell].value = sellOrder[countSell - 1].value;
            }
          }
        }
      }
    }
  }

  callback();
}

var checkKey = function checkKey() {
  keyOrder = [];
  var information = {
    max: 0,
    price: 0,
    buyorsell: false
  }
  for (var i = 0; i < buyOrder.length; i++) {
    if (sellOrder[i].value > buyOrder[buyOrder.length - i - 1].value) {
      keyOrder[i] = buyOrder[buyOrder.length - i - 1];
    } else {
      keyOrder[i] = sellOrder[i];
    }
  }

  for (var i = 0; i < keyOrder.length; i++) {
    if (keyOrder[i].value > information.max) {
      information.max = keyOrder[i].value;
      information.price = keyOrder[i]._id.price;
      information.buyorsell = keyOrder[i]._id.buyorsell;
    }
  }
  return information;
}

var minorFocus = function minorFocus(data, information, callback) {
  var conditions = {};

  if (information.buyorsell) {
    conditions = {
      price: {
        $gte: information.price
      },
      orderBy: 1
    }
  } else {
    conditions = {
      price: {
        $lte: information.price
      },
      orderBy: -1
    }
  }

  Order.find({
    symbol: data.symbol,
    buyorsell: information.buyorsell,
    remaining: {
      $gt: 0
    },
    $or: [{
      price: conditions.price
    }, {
      type: 0
    }]
  }).sort({
    type: 1,
    price: conditions.orderBy,
    time: 1,
    remaining: -1
  }).exec(function(err, data) {
    for (var i = 0; i < data.length; i++) {
      dirtyKLDK.push(data[i]);
      dirtyKLDK[i].remaining = 0;
    }
    callback();
  });
}

var majorFocus = function majorFocus(data1, information, callback) {
  var conditions = {};
  var copyInformation = information;

  if (information.buyorsell) {
    conditions = {
      price: {
        $lte: information.price
      },
      orderBy: 1
    }
  } else {
    conditions = {
      price: {
        $gte: information.price
      },
      orderBy: -1
    }
  }

  Order.find({
    symbol: data1.symbol,
    buyorsell: !information.buyorsell,
    remaining: {
      $gt: 0
    },
    $or: [{
      price: conditions.price
    }, {
      type: 0
    }]
  }).sort({
    type: 1,
    price: conditions.orderBy,
    time: 1,
    remaining: -1
  }).exec(function(err, data) {
    for (var i = 0; i < data.length; i++) {
      if (copyInformation.max > 0) {
        if (copyInformation.max > data[i].remaining) {
          copyInformation.max -= data[i].remaining;
          dirtyKLDK.push(data[i]);
          dirtyKLDK[dirtyKLDK.length - 1].remaining = 0;
        } else {
          dirtyKLDK.push(data[i]);
          dirtyKLDK[dirtyKLDK.length - 1].remaining -= copyInformation.max;
          copyInformation.max = 0;
        }
      }
    }
    callback();
  });
}

exports.mapReduceFunction = mapReduceFunction;
exports.mapReduceATO = mapReduceATO;
exports.newArray = newArray;
exports.augmentOrder = augmentOrder;
exports.checkKey = checkKey;
exports.minorFocus = minorFocus;
exports.majorFocus = majorFocus;