var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var orderSchema = new Schema({
	name : String,
	symbol : String,
	original : Number,
	remaining : Number,
	buyorsell : Boolean,
	type : Number,
	price : Number,
	time : Date,
  phienT: Number,
  gameCode: Number
});

var Order = mongoose.model('order', orderSchema, "order");

module.exports = Order;