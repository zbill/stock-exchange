var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gameSchema = new Schema({
  phienT: Number,
  timeATO: Number,
  timeATC: Number, 
  timeSleep: Number, 
  timeKLTT: Number,
  stepT: Number,
  gameCode: Number,
  status: String,
  timeStart: Number
});

var Game = mongoose.model('game', gameSchema, "game");

module.exports = Game;
