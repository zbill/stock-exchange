var Order = require('./Order');
var Stock = require('./Stock');
var Game = require('./Game');

var addOrder = function addOrder(order, callback) {
  Order.create(order, callback);
}

var getStock = function getStock(callback) {
  Stock.find().exec(callback);
}

var getGameInformation = function getGameInformation (callback) {
  Game.find().exec(callback);
}

exports.getStock = getStock;
exports.addOrder = addOrder;