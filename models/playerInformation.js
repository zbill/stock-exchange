var Order = require('./Order');

var viewOrder = function viewOrder (data, callback) {
  Order.find({
    name : data.name,
    remaining: {
      $gt: 0
    }
  }).sort({
    time: 1
  }).exec(callback);
}

var deleteOrder = function deleteOrder (data, callback) {
  Order.remove({
    _id: data._id
  }).exec(callback);
}

exports.viewOrder = viewOrder;
exports.deleteOrder = deleteOrder;