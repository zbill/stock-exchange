var Group = require('./Group');
var db_Player = require('./db_Player');

var leaveGroup = function leaveGroup(data, callback) {
  Group.remove(data._id, callback);
}

var declineGroup = function declineGroup(data, callback) {}

var listGroup = function listGroup(callback) {
  Group.find(callback);
}

var listOne = function listOne(data , callback){
  Group.find({
    captain : data._id
  }, callback);
}

var addRequestGroup = function addRequestGroup(data, player, callback) {
  Group.find({
    groupName: data.groupName
  }, function(err, group) {
    var notExsit = true;
    for (var i = 0; i < group[0].requestList.length; i++) {
      if (group[0].requestList[i] == player._id) {
        db_Player.addPlayerRequest(player , group[0], callback);
        notExsit = false;
        break;
      }
    }
    if (notExsit) {
      Group.update({
        groupName: data.groupName
      }, {
        $push: {
          requestList: player._id
        }
      }, function(err, group1){
        db_Player.addPlayerRequest(player ,group[0], callback);
      })
    }
  });
}

var removeRequestGroup = function removeRequestGroup(data, player, callback) {
  Group.find({
    groupName: data.groupName
  }, function(err, group) {
    Group.update({
      groupName: data.groupName
    }, {
      $pull: {
        requestList: player._id
      }
    }, function({
      db_Player.removePlayerRequest(player ,group[0], callback);
    }));
  });
}

var addInviteGroup = function addInviteGroup(data, player, callback){
  Group.find({
    groupName: data.groupName
  }, function(err, group){
    var notExsit = true;
    for (var i = 0; i < group[0].inviteList.length; i++) {
      if (group[0].inviteList[i] == player.invite_id) {
        db_Player.addPlayerInvite(player , group[0], callback);
        notExsit = false;
        break;
      }
    }
    if (notExsit) {
      Group.update({
        groupName: data.groupName
      }, {
        $push: {
          inviteList: player.invite_id
        }
      }, function(err, group1){
        db_Player.addPlayerInvite(player ,group[0], callback);
      })
    }
  })
}

var removeInviteGroup = function removeInviteGroup(data, player, callback){
  Group.find({
    groupName: data.groupName
  }, function(err, group) {
    Group.update({
      groupName: data.groupName
    }, {
      $pull: {
        inviteList: player.invite_id
      }
    }, function({
      db_Player.removePlayerInvite(player ,group[0], callback);
    }));
  });
}

var createGroup = function createGroup(data, callback) {
  Group.create(data, callback);
}

exports.listOne = listOne;
exports.listGroup = listGroup;
exports.createGroup = createGroup;
exports.addInviteGroup = addInviteGroup;
exports.addRequestGroup = addRequestGroup;
exports.removeInviteGroup = removeInviteGroup;
exports.removeRequestGroup = removeRequestGroup;