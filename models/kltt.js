var Order = require('./Order');

var findMatchableOrders = function findMatchableOrders(order, callback) {
  var originalOrder = order;
  var conditions = {};

  if (originalOrder.buyorsell) {
    conditions = {
      buyorsell: false,
      price: {
        $lte: originalOrder.price
      },
      orderBy: 1
    };
  } else {
    conditions = {
      buyorsell: true,
      price: {
        $gte: originalOrder.price
      },
      orderBy: -1
    };
  }

  Order.find({
    symbol: originalOrder.symbol,
    buyorsell: !originalOrder.buyorsell,
    $or: [{
      price: conditions.price
    }, {
      type: 0
    }],
    remaining: {
      $gt: 0
    }
  }).sort({
    price: conditions.orderBy,
    time: 1,
    remaining: -1
  }).exec(callback);
}

var matchOrders = function matchOrders(orders, data) {
  var dirty = [];
  // console.log(data);
  if (data.length == 0) {
    dirty.push(orders);
  } else {
    for (var i = 0; i < data.length; i++) {
      if (orders.remaining == 0) {
        return dirty;
      } else if (orders.remaining > 0) {
        if (orders.remaining > data[i].remaining) {
          orders.remaining -= data[i].remaining;
          data[i].remaining = 0;
          dirty.push(data[i]);
          if (i == (data.length - 1)) {
            dirty.push(orders);
          }
        } else {
          data[i].remaining -= orders.remaining;
          orders.remaining = 0;
          dirty.push(data[i]);
          dirty.push(orders);
        }
      }
    }
  }
  return dirty;
}

var saveAll = function saveAll(records, callback) {
  async.forEach(records, function(record, cb) {
    console.log(record);
    if (typeof record.save === "function") {
      record.save(cb);
    } else {
      Order(record).save(cb);
    }
  }, callback);
}

exports.findMatchableOrders = findMatchableOrders;
exports.matchOrders = matchOrders;
exports.saveAll = saveAll;
