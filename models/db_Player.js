var Player = require('./Player');

var addPlayer = function addPlayer(player, callback) {
  Player.create(player, callback);
}

var listPlayer = function listPlayer(callback) {
  Player.find(callback);
}

var listOne = function listOne(data, callback) {
  Player.find({
    userName: data.userName
  }, callback);
}

var addPlayerRequest = function addPlayerRequest(player, group, callback) {
  Player.find({
    _id: player._id
  }, function(err, pl) {
    // console.log(player);
    console.log(group);
    var notExsit = true;
    for (var i = 0; i < pl[0].requestList.length; i++) {
      if (pl[0].requestList[i] == group._id) {
        notExsit = false;
        console.log("exsit")
        callback();
        break;
      }
    }
    if (notExsit) {
      Player.update({
        _id: player._id
      }, {
        $push: {
          requestList: group._id
        }
      }, function(){
        console.log("success")
        callback();
      });
    }
  });
}

var addPlayerInvite = function addPlayerInvite(player, group, callback){
  Player.find({
    _id: player.invite_id
  }, function(err, pl) {
    // console.log(player);
    console.log(group);
    var notExsit = true;
    for (var i = 0; i < pl[0].inviteList.length; i++) {
      if (pl[0].inviteList[i] == group._id) {
        notExsit = false;
        console.log("exsit")
        callback();
        break;
      }
    }
    if (notExsit) {
      Player.update({
        _id: player.invite_id
      }, {
        $push: {
          inviteList: group._id
        }
      }, function(){
        console.log("success")
        callback();
      });
    }
  });
}

var removePlayerRequest = function removePlayerRequest(player, group, callback){
  Player.find({
    _id : player.invite_id
  }, function(err, pl){
    Player.update({
      _id : player.invite_id
    }, {
      $pull: {
        inviteList: group._id
      }
    }, callback);
  });
}

var removePlayerInvite = function removePlayerInvite(player, group, callback){
  Player.find({
    _id : player._id
  }, function(err, pl){
    Player.update({
      _id : player._id
    }, {
      $pull: {
        requestList: group._id
      }
    }, callback);
  });
}


exports.listOne = listOne;
exports.addPlayer = addPlayer;
exports.listPlayer = listPlayer;
exports.addPlayerInvite = addPlayerInvite;
exports.addPlayerRequest = addPlayerRequest;
exports.removePlayerInvite = removePlayerInvite;
exports.removePlayerRequest = removePlayerRequest;