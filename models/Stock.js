var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var stockSchema = new Schema({
  name: String,
  symbol: String,
  san: Number,
  tran: Number,
  price: Number,
  stepPrice: Number,
  stepLength: Number,
  gameCode: Number
});
var Stock = mongoose.model('stock', stockSchema, "stock");

module.exports = Stock;