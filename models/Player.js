var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var playerSchema = new Schema({
  userName: String,
  password: String,
  balance: Number,
  group: String,
  inviteList: [{type: String}],
  requestList: [{type: String}],
  gameCode: Number
});

var Player = mongoose.model('player', playerSchema, "player");

module.exports = Player;