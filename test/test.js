var testCase = require('mocha').describe
var pre = require('mocha').before
var assertions = require('mocha').assertions
var assert = require('assert')

var order = {
  name: 1,
  symbol: 2,
  original: 3,
  remaining: 4,
  buyorsell: 5,
  type: 6,
  price: 7,
  time: new Date()
};

// describe('handle message function', function(){
//   it('should return -1', function(){
//     [1,2,3].indexOf(4).should.equal(-1);
//   })
// })

describe('Array', function(){
  describe('#indexOf()', function(){
    it('should return -1 when the value is not present', function(){
      assert.equal(-1, [1,2,3].indexOf(5));
    })
  })
})
